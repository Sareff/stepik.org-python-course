a=[int(i) for i in input().split()]
a.sort()
for i in set(a): # <- here it is
    if a.count(i) > 1: print(i, end=' ')

# While I'm googled keys to solution this problem I came across one interesting thing: set function. (4 line)
# As I understand, set function founds unique elements of list and any elements that repeats in list,
# just cuts from list.
# So in output we gain list with only unique elements. All genious is simple :)