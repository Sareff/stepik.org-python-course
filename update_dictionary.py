def update_dictionary(d, key, value):
	if key not in d:
		if 2*key is d:
			d[2*key] += [value]
		elif (2*key is not d) and (d.get(2*key)==None):
			d[2*key] = []
			d[2*key] += [value]
		elif (2*key is not d) and (d.get(2*key)!=None):
			d[2*key] += [value]
	else:
		d[key] += [value]

d = {}
print(update_dictionary(d, 1, -1))  # None
print(d)                            # {2: [-1]}
update_dictionary(d, 2, -2)
print(d)                            # {2: [-1, -2]}
update_dictionary(d, 1, -3)
print(d)                            # {2: [-1, -2, -3]}