'''
a, n, c = [], int(input()), 1
for i in range(1, n+1):
    for j in range(c):
        a.append(c)
    c += 1
    if len(a) >= n:
        break
print(*a[:n])

# I completed this problem after I saw solution from StracOwerflow
# so here it is:
'''
n, v = int(input()), []
for i in range(1, n+1):
    c = min(n, i)
    n = n - c
    v += [str(i)] * c
    if n <= 0:
        break
print(" ".join(v))

#in terms of size they are the same, but I like second more